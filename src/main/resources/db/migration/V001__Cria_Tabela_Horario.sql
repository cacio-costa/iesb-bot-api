create table bot_api.horario (
	id serial not null,
	data date not null,
	hora time not null,
	disponivel boolean not null default true,
	paciente text,
	convenio text,
	
	primary key (id),
	constraint data_hora_unicos unique(data, hora)
);