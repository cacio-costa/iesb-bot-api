package estudos.iesb.botapi.web;

import java.util.List;

import estudos.iesb.botapi.modelo.Horario;

public class DisponibilidadesDto {

	public List<Horario> encontrados;
	public List<Horario> alternativas;
	
	public DisponibilidadesDto(List<Horario> encontrados, List<Horario> alternativas) {
		this.encontrados = encontrados;
		this.alternativas = alternativas;
	}


	public static DisponibilidadesDto naDataEscolhida(List<Horario> horarios) {
		return new DisponibilidadesDto(horarios, null);
	}
	
	public static DisponibilidadesDto outrasOpcoes(List<Horario> horarios) {
		return new DisponibilidadesDto(null, horarios);
	}
	
}
