package estudos.iesb.botapi.web;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import estudos.iesb.botapi.modelo.Horario;
import estudos.iesb.botapi.modelo.HorarioRepository;

@Controller
public class AgendaController {
	
	@Autowired
	private HorarioRepository horarios;

	@GetMapping("/datas-disponiveis")
	public ResponseEntity<List<String>> datasDisponiveis() {
		return ResponseEntity.ok(horarios.proximasDatasDisponiveis(LocalDate.now()));
	}
	
	@GetMapping("/horarios-disponiveis")
	public ResponseEntity<List<String>> horariosDisponiveis(@RequestParam String data) {
		LocalDate dataEscolhida = LocalDate.parse(data);
		
		return ResponseEntity.ok(horarios.disponiveisEm(dataEscolhida));
	}
	
	@ResponseBody
	@PostMapping("/marcacao")
	public String marcacao(Horario horario, HttpServletResponse resp) {
		if (horario.getData() == null) {
			resp.setStatus(400);
			return "Parâmetro \"data\" não informado.";
		}
		
		if (horario.getHora() == null) {
			resp.setStatus(400);
			return "Parâmetro \"hora\" não informado.";
		}
		
		String dataFormatada = DateTimeFormatter.ofPattern("dd/MM/yyyy (E)").format(horario.getData());
		String horaFormatada = DateTimeFormatter.ofPattern("HH:mm").format(horario.getHora());
		
		Horario horarioEscolhido = horarios.findByDataAndHora(horario.getData(), horario.getHora());
		if (horarioEscolhido == null) {
			resp.setStatus(404);
			return String.format("Horário escolhido para %s às %s não encontrado", dataFormatada, horaFormatada);
		}
		
		horarioEscolhido.revervaPara(horario.getPaciente(), horario.getConvenio());
		Horario horarioSalvo = horarios.save(horarioEscolhido);

		String convenioOuParticular = ", convênio " + horarioSalvo.getConvenio();
		if (horario.isParticular()) {
			convenioOuParticular = ", consulta particular.";
		}
		
		String mensagem = String.format(
			"Horário agendado para %s às %s%s",
			dataFormatada,
			horaFormatada,
			convenioOuParticular
		);
		
		return mensagem;
	}
	
}
