package estudos.iesb.botapi.modelo;

import java.time.LocalDate;
import java.time.LocalTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Table("bot_api\".\"horario")
public class Horario {

	@Id
	private Long id;
	
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate data;
	
	@DateTimeFormat(iso = ISO.TIME)
	private LocalTime hora;
	
	private boolean disponivel;
	
	private String paciente;
	private String convenio;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public LocalTime getHora() {
		return hora;
	}

	public void setHora(LocalTime hora) {
		this.hora = hora;
	}

	public boolean isDisponivel() {
		return disponivel;
	}

	public void setDisponivel(boolean disponivel) {
		this.disponivel = disponivel;
	}

	public String getPaciente() {
		return paciente;
	}

	public void setPaciente(String paciente) {
		this.paciente = paciente;
	}

	public String getConvenio() {
		return convenio;
	}

	public void setConvenio(String convenio) {
		this.convenio = convenio;
	}

	public void revervaPara(String paciente, String convenio) {
		this.convenio = convenio;
		this.paciente = paciente;
		this.disponivel = false;
	}

	public boolean isParticular() {
		return convenio == null;
	}

}
