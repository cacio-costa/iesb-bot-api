package estudos.iesb.botapi.modelo;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;

public interface HorarioRepository extends CrudRepository<Horario, Long> {

	@Query("select distinct to_char(data, 'dd/mm/yyyy') as data from bot_api.horario where disponivel = true and data >= :aPartirDe order by data limit 7")
	List<String> proximasDatasDisponiveis(LocalDate aPartirDe);

	@Query("select distinct to_char(hora, 'hh24:mi') as hora from bot_api.horario where disponivel = true and data = :data order by hora")
	List<String> disponiveisEm(LocalDate data);

	Horario findByDataAndHora(LocalDate data, LocalTime hora);
	
}
