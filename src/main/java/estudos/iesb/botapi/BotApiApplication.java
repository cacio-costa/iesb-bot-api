package estudos.iesb.botapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;

@SpringBootApplication
@EnableJdbcRepositories
public class BotApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BotApiApplication.class, args);
	}

}
